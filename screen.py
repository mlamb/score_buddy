import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

SCREEN_WIDTH=128
SCREEN_HEIGHT=64

large_font = ImageFont.truetype('font/Minecraftia.ttf', 16)
small_font = ImageFont.truetype('font/Minecraftia.ttf', 8)
disp = Adafruit_SSD1306.SSD1306_128_64(rst=None)

# Initialize library.
disp.begin()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
image = Image.new('1', (SCREEN_WIDTH, SCREEN_HEIGHT))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

def clear():
    """ Clear the display """
    disp.clear()
    disp.display()


def show_title():
    """ Show the title screen """
    title="Score Buddy"
    subtitle="Press a key to continue..."

    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,SCREEN_WIDTH,SCREEN_HEIGHT), outline=0, fill=0)

    # Add our two lines of text
    add_text(title, x=0, y=16, font=large_font)
    add_text(subtitle, x=0, y=48, font=small_font)

    # Display
    disp.image(image)
    disp.display()


def show_score(hp, vp, hg, vg, hs, vs):
    """ Show the score card on the screen """
    set_col = 17
    game_col = 54
    point_col = 91
    home_row = 14
    visit_row = 43

    draw.rectangle((0,0,SCREEN_WIDTH,SCREEN_HEIGHT), outline=0, fill=0)

    add_text('Sets', x=set_col, y=0, font=small_font)
    add_text('Games', x=game_col, y=0, font=small_font)
    add_text('Points', x=point_col, y=0, font=small_font)

    add_text('H', x=0, y=home_row, font=large_font)
    add_text('V', x=0, y=visit_row, font=large_font)

    # Show the set score
    add_text(hs, x=set_col, y=home_row, font=large_font) 
    add_text(vs, x=set_col, y=visit_row, font=large_font) 

    # Show the game score
    add_text(hg, x=game_col, y=home_row, font=large_font) 
    add_text(vg, x=game_col, y=visit_row, font=large_font) 

    # Show the point score
    add_text(hp, x=point_col, y=home_row, font=large_font) 
    add_text(vp, x=point_col, y=visit_row, font=large_font) 

    # Display
    disp.image(image)
    disp.display()


def show_text(text1, text2):
    """ Show two rows of text on the screen. """
    draw.rectangle((0,0,SCREEN_WIDTH,SCREEN_HEIGHT), outline=0, fill=0)

    add_text(text1, x=0, y=8, font=large_font)
    add_text(text2, x=0, y=40, font=large_font)

    # Display
    disp.image(image)
    disp.display()


def add_text(value, x, y, font):
    """ Add a line of text to the screen, at position x,y using font.
    The text will be drawn over the top of text that is already on the
    screen. """
    draw.text((x,y), value, font=font, fill=255)

