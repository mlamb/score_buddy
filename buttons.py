from gpiozero import Button
from subprocess import check_call
import time

# Mapping for all the buttons
home_button = Button(6)
visitor_button = Button(5)
up_button = Button(17)
down_button = Button(22)
left_button = Button(27)
right_button = Button(23)


# The completer class is used to capture button presses.
# Each time a button is pressed the complete() method is called
# which checks which button was pressed and figures out if it was the
# home or visitor button.
#
# This is a little complex but there was no easy way to wait for any button
# to be pressed using the gpiozero library.
#
class Completer(object):
    def __init__(self):
        self.done = False
        self.home_value = False
        self.visitor_value = False

    def complete(self, btn):
        """ Method is called whenever a button is pressed.
        The passed in btn should be one of the buttons instantiated
        at the top of this file.
        """
        # set the done flag so that is_complete will return True
        self.done = True
        # Set home_value to True if the button was the home_button
        self.home_value = (btn == home_button)
        # Set visitor_value to True if the button was the visitor_button
        self.visitor_value = (btn == visitor_button)

    @property
    def is_complete(self):
        """ Return True if complete has been called otherwise false. """
        return self.done


def enable_shutdown_button(callback):
    """ Enable the shutdown button.
    When the down button is held for a few seconds the computer will
    shutdown.
    """
    def _shutdown():
        callback()
        check_call(['sudo', 'poweroff'])
    down_button.when_held = _shutdown


def wait_for_any_key():
    """ Wait for either the home or visitor button to be pressed.
    Returns the home and visitor buttons as booleans.
    """
    # Construct the completer.
    # The c.is_complete property will be false until a button is
    # pressed.
    c = Completer()
    
    # Whenever the home or visitor button is pressed, call the
    # complete() method passing in the button that was pressed.
    home_button.when_released = lambda btn : c.complete(btn)
    visitor_button.when_released = lambda btn : c.complete(btn)
    
    # Wait for a button to be pressed
    while not c.is_complete:
        # Sleep for 0.1 of a second to allow the computer
        # to do other things whilst we wait for the button.
        time.sleep(0.1)

    # A button has been pressed. Return the state of the 
    # home and visitor buttons.
    return c.home_value, c.visitor_value
