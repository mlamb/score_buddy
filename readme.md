# Score Buddy Rasperry Pi Demo

This is an implementation of the Score Buddy Pseudocode in python to run on the Rasperry PI Zero.

It uses the Adafruit 128x64 OLED Bonnet for buttons and display - See www.adafruit.com/product/3531.

To run from the command line type 'python score_buddy.py'
