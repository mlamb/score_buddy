from __future__ import print_function
import buttons
import screen
import time


# Home Points
hp = 0

# Visitor Points
vp = 0

# Home Games
hg = 0

# Visitor Games
vg = 0

# Home Sets
hs = 0

# Visitor Sets
vs = 0

# Tie break points home
tbph = 0

# Tie break points visitor
tbpv = 0

# The current state of the game, one of the following:
#  NUMBER: a game is in progress, prior to DEUCE
#  DEUCE: a game is at DEUCE
#  ADV_VISITOR/ADV_HOME: a game is at advantage
#  TIE_BREAK: a tie break is in progress
#  WIN_HOME/WIN_VISITOR: a game has just been won
#  WIN_SET_H/WIN_SET_V: a set has just been won
#  WIN: the match has just been completed and won.
gamestate = "NUMBER"


def run_game():
    """ Run a single game.
    This function will not exit until the game is complete.
    See the comments below for more detail.
    """

    # First reset the game variables to start a new game
    reset()

    # Keep looping until the gamestate reaches WIN which indicates
    # a player has won.
    while gamestate != "WIN":
        # Show the score board
        show_score()

        # Wait for a key to be pressed
        # The program will pause here forever until a key is pressed
        h_button_is_pressed, v_button_is_pressed = buttons.wait_for_any_key()

        if gamestate == "TIE_BREAK":
            # If there is a tie break underway then run the tie break
            # scoring logic.
            tie_break_scoring(h_button_is_pressed, v_button_is_pressed)
        else:
            # No tie break is underway therefore run the normal 
            # game scoring logic.
            single_game_scoring(h_button_is_pressed, v_button_is_pressed)
            game_scoring(h_button_is_pressed, v_button_is_pressed)

        # Now update the set score if required
        set_scoring(h_button_is_pressed, v_button_is_pressed)


def single_game_scoring(h_button_is_pressed, v_button_is_pressed):
    """ Update the current game score.
    This is based on the pseudo-code pages 2,3 and 4.
    This function will update the gamestate to either
      WIN_HOME/WIN_VISITOR if the game is won.
    """

    global hp
    global vp
    global gamestate

    # Instructions for h button being pressed
    if hp == 0 and h_button_is_pressed:
        hp = 15

    elif hp == 15 and h_button_is_pressed:
        hp = 30

    elif hp == 30 and h_button_is_pressed:
        hp = 40

    elif hp == 40 and vp <= 30 and h_button_is_pressed:
        gamestate = "WIN_HOME"
        hp = 0
        vp = 0

    # Instructions for v button being pressed
    if vp == 0 and v_button_is_pressed:
        vp = 15

    elif vp == 15 and v_button_is_pressed:
        vp = 30

    elif vp == 30 and v_button_is_pressed:
        vp = 40

    elif vp == 40 and hp <= 30 and v_button_is_pressed:
        gamestate = "WIN_VISITOR"
        hp = 0
        vp = 0

    # Instructions for Scoring Deuce
    if hp == 40 and vp == 40 and gamestate is "NUMBER":
        gamestate = "DEUCE"

    elif gamestate == "DEUCE":
        if v_button_is_pressed:
            gamestate = "ADV_VISITOR"
        elif h_button_is_pressed:
            gamestate = "ADV_HOME"

    elif gamestate == "ADV_HOME":
        if h_button_is_pressed:
            gamestate = "WIN_HOME"
            hp = 0
            vp = 0
        elif v_button_is_pressed:
            gamestate = "DEUCE"

    elif gamestate == "ADV_VISITOR":
        if h_button_is_pressed:
            gamestate = "DEUCE"
        elif v_button_is_pressed:
            gamestate = "WIN_VISITOR"
            hp = 0
            vp = 0


def game_scoring(h_button_is_pressed, v_button_is_pressed):
    """ Update the game score - hg and vg.
    This is based on the pseudo-code pages 5, 6 and 7.
    The if statements below ensure that the gamestate is either WIN_HOME
    or WIN_VISITOR before doing anything.
    """

    global hg
    global vg
    global tbph
    global tbpv
    global gamestate

    # Instructions if WIN_HOME
    if hg == 0 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 1
        gamestate = "NUMBER"
    
    elif hg == 1 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 2
        gamestate = "NUMBER"

    elif hg == 2 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 3
        gamestate = "NUMBER"

    elif hg == 3 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 4
        gamestate = "NUMBER"

    elif hg == 4 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 5
        gamestate = "NUMBER"

    elif hg == 5 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 6
        gamestate = "NUMBER"

    elif hg == 6 and h_button_is_pressed and gamestate == "WIN_HOME":
        hg = 7
        gamestate = "NUMBER"

    # Instructions if WIN_VISITOR
    if vg == 0 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 1
        gamestate = "NUMBER"
    
    elif vg == 1 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 2
        gamestate = "NUMBER"

    elif vg == 2 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 3
        gamestate = "NUMBER"

    elif vg == 3 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 4
        gamestate = "NUMBER"

    elif vg == 4 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 5
        gamestate = "NUMBER"

    elif vg == 5 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 6
        gamestate = "NUMBER"

    elif vg == 6 and v_button_is_pressed and gamestate == "WIN_VISITOR":
        vg = 7
        gamestate = "NUMBER"

    if hg == 6 and vg < 5:
        gamestate = "WIN_SET_H"

    elif hg == 7 and vg <= 5:
        gamestate = "WIN_SET_H"

    elif vg == 6 and hg < 5:
        gamestate = "WIN_SET_V"

    elif vg == 7 and hg <= 5:
        gamestate = "WIN_SET_V"

    elif hg == 6 and vg == 6:
        display("TIE BREAK")
        tbph = 0
        tbpv = 0
        gamestate = "TIE_BREAK"


def tie_break_scoring(h_button_is_pressed, v_button_is_pressed):
    """ Update the tie break score - tbph and tbpv.
    This is based on the pseudo-code pages 8, 9 and 10.
    Only call this function if the current state is TIE_BREAK
    """
    global tbph
    global tbpv
    global gamestate


    if tbph == 0 and h_button_is_pressed:
        tbph = 1

    elif tbph == 1 and h_button_is_pressed:
        tbph = 2

    elif tbph == 2 and h_button_is_pressed:
        tbph = 3

    elif tbph == 3 and h_button_is_pressed:
        tbph = 4

    elif tbph == 4 and h_button_is_pressed:
        tbph = 5

    elif tbph == 5 and h_button_is_pressed:
        tbph = 6

    elif tbph == 6 and tbpv <= 5 and h_button_is_pressed:
        gamestate = "WIN_SET_H"

    elif tbph - tbpv >= 2 and h_button_is_pressed:
        gamestate = "WIN_SET_H"

    elif tbpv == 0 and v_button_is_pressed:
        tbpv = 1

    elif tbpv == 1 and v_button_is_pressed:
        tbpv = 2

    elif tbpv == 2 and v_button_is_pressed:
        tbpv = 3

    elif tbpv == 3 and v_button_is_pressed:
        tbpv = 4

    elif tbpv == 4 and v_button_is_pressed:
        tbpv = 5

    elif tbpv == 5 and v_button_is_pressed:
        tbpv = 6

    elif tbpv == 6 and tbph <= 5 and v_button_is_pressed:
        gamestate = "WIN_SET_V"

    elif tbpv - tbph >= 2 and v_button_is_pressed:
        gamestate = "WIN_SET_V"


def set_scoring(h_button_is_pressed, v_button_is_pressed):
    """ Update the set score - hs and vs.
    If a set has been won then hg and vg are reset to 0.
    This is based on the pseudo-code pages 11, 12, 13 and 14.
    """
    global hs
    global vs
    global hg
    global vg
    global gamestate

    # Instructions for 1st Set Scoring
    if hs == 0 and gamestate == "WIN_SET_H":
        hs = 1
        display("Set 1","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 0 and gamestate == "WIN_SET_V":
        vs = 1
        display("Set 1","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    
    # Instructions for 2nd Set Scoring
    elif hs == 0 and vs == 1 and gamestate == "WIN_SET_H":
        hs = 1
        display("Set 2","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif hs == 1 and vs == 0 and gamestate == "WIN_SET_H":
        hs = 2
        display("Set 2","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 0 and hs == 1 and gamestate == "WIN_SET_V":
        vs = 1
        display("Set 2","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 1 and hs == 0 and gamestate == "WIN_SET_V":
        vs = 2
        display("Set 2","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"

    # Instructions for 3rd Set Scoring
    elif hs == 0 and vs == 2 and gamestate == "WIN_SET_H":
        hs = 1
        display("Set 3","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif hs == 1 and vs == 1 and gamestate == "WIN_SET_H":
        hs = 2
        display("Set 3","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif hs == 2 and vs == 0 and gamestate == "WIN_SET_H":
        hs = 3
        display("Game Over","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"
    elif vs == 0 and hs == 2 and gamestate == "WIN_SET_V":
        vs = 1
        display("Set 3","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 1 and hs == 1 and gamestate == "WIN_SET_V":
        vs = 2
        display("Set 3","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 2 and hs == 0 and gamestate == "WIN_SET_V":
        vs = 3
        display("Game Over","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"

    # Instructions for 4th Set Scoring
    elif hs == 1 and vs == 2 and gamestate == "WIN_SET_H":
        hs = 2
        display("Set 4","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif hs == 2 and vs == 1 and gamestate == "WIN_SET_H":
        hs = 3
        display("Game Over","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"
    elif vs == 1 and hs == 2 and gamestate == "WIN_SET_V":
        vs = 2
        display("Set 4","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "NUMBER"
    elif vs == 2 and hs == 1 and gamestate == "WIN_SET_V":
        vs = 3
        display("Game Over","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"

    # Instructions for 5th Set Scoring
    elif hs == 2 and vs == 2 and gamestate == "WIN_SET_H":
        hs = 3
        display("Game Over","Home Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"
    elif vs == 2 and hs == 2 and gamestate == "WIN_SET_V":
        vs = 3
        display("Game Over","Visitor Wins!")
        hg = 0
        vg = 0
        gamestate = "WIN"


def reset():
    """ Reset the internal state of the game. 
    This can be used to start a new game.
    """
    global hp
    global vp
    global hs
    global vs
    global hg
    global vg
    global tbph
    global tbpv
    global gamestate
    hp = 0
    vp = 0
    hg = 0
    vg = 0
    hs = 0
    vs = 0
    tbph = 0
    tbpv = 0
    gamestate = "NUMBER"


def show_score():
    """ Show the score on the display.
    """

    # Create the text string to use as the home and visitor points.
    if gamestate == "ADV_HOME":
        # If the game is at advantage to home then show
        # home points as 'AD' and visitor points as 40.
        h_text = "AD"
        v_text = "40"
    elif gamestate == "ADV_VISITOR":
        # If the game is at advantage to visitor then show
        # visitor points as 'AD' and home points as 40.
        h_text = "40"
        v_text = "AD"
    elif gamestate == "TIE_BREAK":
        # If the game is a tie breaker then show the points
        # as the current tie break points.
        # The following line will show the tbph value which is a number
        # from 0 to 7 as 'T1', 'T2', etc.
        h_text = "T%s" % tbph
        v_text = "T%s" % tbpv
    else:
        # Otherwise just show the home and visitor points as the number
        # e.g. if hp is 15 then show the score as '15'
        h_text = str(hp)
        v_text = str(vp)

    # Pass all the points, game and set score to show_score
    # to display it on the screen.
    screen.show_score(h_text,v_text,str(hg),str(vg),str(hs),str(vs))


def display(text1, text2=""):
    """ Display some temporary text on the screen.
    This is used when a set or a match is won, to inform the user.
    The text will be shown on the screen and the program will pause
    for 2 seconds.
    """
    screen.show_text(text1, text2)
    # Pause the program for 2 seconds.
    time.sleep(2)


if __name__ == "__main__":
    # This is the code that is run when the program is first started.

    # Do some button initialisation
    buttons.enable_shutdown_button(screen.clear)

    while True:
        # Show the 'Score Buddy' title screen
        screen.show_title()

        # Wait for the user to press a key before continuing
        buttons.wait_for_any_key()

        # Now run the game
        run_game()

